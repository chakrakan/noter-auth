package com.noter.auth.dtos;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserInfoDto {
    private String userName;
    private String displayName;
    private String password;
    private List<String> roles;
    private String email;
    private Date createdAt;
    private Date modifiedAt;
}
