package com.noter.auth.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class RoleDto {
    private String role;
    private Date createdAt;
    private Date modifiedAt;

}
