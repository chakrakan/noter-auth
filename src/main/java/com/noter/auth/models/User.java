package com.noter.auth.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@Document(collection = "users")
public class User extends GenericModel {
    @Indexed(direction = IndexDirection.DESCENDING, unique = true)
    private String userName;

    @Indexed(direction = IndexDirection.DESCENDING, unique = true)
    private String displayName;

    @Indexed(direction = IndexDirection.DESCENDING, unique = true)
    private String email;

    private String password;
    private String jwtToken;
    private List<String> roles;

    public User() {
        super();
    }
}
