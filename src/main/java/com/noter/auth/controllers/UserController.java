package com.noter.auth.controllers;

import com.noter.auth.dtos.UserInfoDto;
import com.noter.auth.dtos.UserLoginDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import static com.google.common.base.Preconditions.checkNotNull;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    // Create user
    @PostMapping("/create")
    public UserInfoDto createUser(@RequestBody UserInfoDto userInfoDto) {
        checkNotNull(userInfoDto);
        System.out.println(userInfoDto.getDisplayName());
        System.out.println(userInfoDto.getRoles());
        // Todo add service to handle logic
        return userInfoDto;
    }

    // Get user
    @GetMapping("/info/{userId}")
    public UserInfoDto getUserInfo(@PathVariable("userId") String userId) {
        // Todo add service to handle logic
        System.out.println("get info for user " + userId);
        return null;
    }

    // Login user
    @PostMapping("/login")
    public UserInfoDto loginUser(@RequestBody UserLoginDto userLoginDto) {
        checkNotNull(userLoginDto);
        System.out.println(userLoginDto.getUserName());
        System.out.println(userLoginDto.getPassword());
        return null;
    }

    // Delete user
    // Update user
}
