package com.noter.auth.controllers;

import com.noter.auth.dtos.RoleDto;
import org.springframework.web.bind.annotation.*;

import static com.google.common.base.Preconditions.checkNotNull;

@RestController
@RequestMapping("/role")
public class RoleController {
    // Create role
    @PostMapping("/create")
    public RoleDto createUser(@RequestBody RoleDto roleDto) {
        checkNotNull(roleDto);
        System.out.println(roleDto.getRole());
        // Todo add service to handle logic
        return null;
    }

    // Get role
    @GetMapping("/info/{roleId}")
    public RoleDto getUserInfo(@PathVariable("roleId") String roleId) {
        // Todo add service to handle logic
        System.out.println("get info for role " + roleId);
        return null;
    }

    // Delete role
    // Update role


}
